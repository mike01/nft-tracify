# NFT tracify
This shellscript helps to enable tracing on specific rules in nft rulesets using minimal dependencies. 

<p align="center">
  <img src="https://gitlab.com/mike01/nft-tracify/-/raw/master/screenshot.png" alt="nft-tracify"/>
</p>

Use cases:

  * Non working firewall rules (blocked packets)


# Dependencies

  *  dialog (https://invisible-island.net/dialog/)


# Usage
`./tracify.sh [tracify|restore]`

1. Select nft rules to enable tracing for: `./tracify.sh tracify`
2. Observe tracing: `nft monitor trace`
3. Goto 1. or restore rules via `./tracify.sh restore`

