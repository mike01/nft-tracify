#!/bin/bash
FILENAME_RULES_CURRENT="nft_rules_original.txt"
FILENAME_RULES_TRACIFIED="nft_rules_tracified.txt"
NFT_ACTIONS="accept|drop|jump|reject|log|snat|dnat|masquerade|meta|queue|dup|counter|notrack"

if [ $# == 0 ]; then
	echo "Usage: $0 [tracify|restore]"
	exit 1
fi

if [ $1 == "tracify" ]; then
	# See code below
	:
elif [ $1 == "restore" ]; then
	if [ -f $FILENAME_RULES_CURRENT ]; then
		nft flush ruleset
		nft -f $FILENAME_RULES_CURRENT
	else
		echo "Did not find old rules, can't restore!"
	fi
	exit 0
else
	exit 1
fi




if [ -f $FILENAME_RULES_CURRENT ]; then
	echo "Using original rules found in $FILENAME_RULES_CURRENT, remove to re-read from nftables"
else
	echo "Storing current (original) rules to $FILENAME_RULES_CURRENT"
	nft list ruleset > $FILENAME_RULES_CURRENT
fi
echo "Press enter to continue"
read

# Preserve trailing whitespace
function nft_to_lines() {
	local cnt=0

	while IFS="" read -r p || [ -n "$p" ]
	do
		cnt=$(($cnt+1))
		# Preserve " in output
		p=$(echo "$p" | sed 's/\"/\\"/g')
		printf '%3d "%s" off\n ' $cnt "$p"
	done < $FILENAME_RULES_CURRENT
}

nft_dialog=$(nft_to_lines $1)
cmd="dialog --checklist \"Select lines via [Space] to enable logging for...\" 0 140 100 ${nft_dialog} 3>&1 1>&2 2>&3"
line_nums=$(eval $cmd)
clear
# Manual line number entry for testing
#line_nums="4 5"
# Create array: "1 2 3" = [1, 2, 3]
IFS=' ' read -r -a line_nums_arr <<< "$line_nums"

num_current=0
> $FILENAME_RULES_TRACIFIED

while IFS='' read -r line || [ -n "${line}" ]; do
	is_sel=0
	num_current=$(($num_current+1))

	if [[ " ${line_nums_arr[@]} " =~ " ${num_current} " ]]; then
		is_sel=1
	fi

	if echo "$line" | grep -q -P "\t{1}\}$"; then
		# End of chain definition: enable tracing
		echo "                meta nftrace set 1" >> $FILENAME_RULES_TRACIFIED
	elif echo "$line" | grep -q -P ".+\{$"; then
		# Ignore start of tables, chains etc
		:
	elif echo "$line" | grep -q -P " hook "; then
		# Ignore initial chain rules (hook definition, policy
		:
	elif [ $is_sel == 1 ]; then
		# ...[action] -> ...nftrace set 1 [action]
		if echo "$line" | grep -q -P "($NFT_ACTIONS)"; then
			#echo "Found an action in: $line"
			line=$(echo "$line" | sed -E 's/('$NFT_ACTIONS')/nftrace set 1 \1/g')
		else
			# Append to end. Warning: use can possibly select "wrong" lines (tracing not applicable)
			line="$line nftrace set 1"
		fi
	fi

	echo "$line" >> $FILENAME_RULES_TRACIFIED
done < $FILENAME_RULES_CURRENT

echo "Applying tracified rules from $FILENAME_RULES_TRACIFIED"
nft flush ruleset
nft -f $FILENAME_RULES_TRACIFIED
echo "Now start tracing via 'nft monitor trace'"
